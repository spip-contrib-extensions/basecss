<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-basecss
// Langue: fr
// Date: 13-06-2016 15:57:19
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'basecss_description' => 'Ce framework CSS pour SPIP fournit une base de travail pour l\'intégration, sous la forme d\'un jeu de feuilles de style dynamiques et paramétrables, proposant les préréglages fondamentaux.',
	'basecss_slogan' => 'Framework CSS pour SPIP',
);
?>